#!/bin/sh

# Scripts/hooks
run_script() {
    script="${1}"
    shift
    args="${@}"
    if [ -f "/aria/scripts/${script}" ]; then
        if [ $# -gt 0 ]; then
            echo "[+] Running '${script}' script (args: ${args})"
        else
            echo "[+] Running '${script}' script"
        fi
        /aria/scripts/${script} ${args}
    else
        echo "[-] Script '${script}' not found"
    fi
}

# Logging
debug() {
    echo "[?]   $@" >&2
}

info() {
    echo "[+]   $@" >&2
}

warn() {
    echo "[-]   $@" >&2
}

fatal() {
    echo "[-]   $@" >&2
    exit -1
}
