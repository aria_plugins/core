all:	build

build:
	fig build

run:
	fig stop
	fig up -d
	
status:
	fig ps
	fig logs
