#!/bin/bash

BRANCH=${BRANCH:-master}

wget https://gitlab.com/aria_plugins/core/raw/${BRANCH}/mk/aria-plugin.mk
grep aria-plugin.mk .gitignore 2>/dev/null || echo aria-plugin.mk >> .gitignore
