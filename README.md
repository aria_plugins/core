A.R.I.A Plugins Core
============

Set of tools to create plugins on A.R.I.A.

We worked hard to make the plugins runnable on any machine running docker
and we are still working hard to make the publishing as easy as possible.

This repository will help you to begin in the plugin-writing, don't hesitate to ask anything, or open an [issue](https://gitlab.com/aria_plugins/core/issues).

---
